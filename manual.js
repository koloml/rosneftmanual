const ARCHIVE_LASTID_NAME = "lastid";
const ARCHIVE_SIZE = 25;
const BACK_BUTTON_ID = "go_back";
const DIRECTORY_BUTTONS = "#buttons";
const DOCUMENT_ID_PREFIX = "#d_"
const ELEMENT_DIRECTORY = "#directory";
const ELEMENT_VIEW = "#view";
const ELEMENT_WORK = "#work";
const ELEMENT_TOP = "#top";
const ELEMENT_ARCHIVE = "#archive";
const ELEMENT_ARCHIVE_VIEW = "#archive-view";
const INPUT_FORM = "#input-values";
const MOBILE_BUTTON = "mobile-button";
const PREWORK_BUTTONS_LIST = "#click-something";
const PREWORK_WINDOW = "#choose-something";
const RECORD_PREFIX = "manual-save-";
const RESULTS = "#results";
const SCHEME_TYPE_ID = "#scheme-types";
const SELECTED = "selected";
const SHOW_ARHIVE_ID = "#show-archive";
const SHOW_WORK_ID = "#show-currentwork";
const SPLIT_SYMBOL = ",";
const NOTIFICATION = "#notify";
const NOTIFICATION_TEXT = NOTIFICATION + " div";

window.onload = function() {
	manual.init();
}
var manual = {
	init: function() {
		hide(document.body);
		manual.top.element = document.querySelector(ELEMENT_TOP);
		manual.top.make();
		with(manual.page) {
			directory.element = document.querySelector(ELEMENT_DIRECTORY);
			directory.makeList([]);
			view.element = document.querySelector(ELEMENT_VIEW);
			currentWork.element = document.querySelector(ELEMENT_WORK);
			hide(view.element);
			hide(currentWork.element);
			show(directory.element);
		}
		show(document.body);
		document.body.className = "show";
		manual.page.currentWork.init();
		manual.page.currentWork.archive.init();
		// fake notification
		document.querySelector(NOTIFICATION).style.display = "none";
		checkUpdates();
	},
	top: {
		element: null,
		make: function() {
			var tabs = data.tabs;
			for(var i = 0; i < tabs.length; i++) {
				var text = tabs[i].text;
				var clickHandler = function(e) {
					manual.top.switchTab(this);
					var tabId = +this.getAttribute("tabid");
					data.tabs[tabId].click();
				}
				var tabButton = document.createElement("a");
				tabButton.setAttribute("tabid", i);
				tabButton.innerText = text;
				tabButton.onclick = clickHandler;
				if(i == 0) {
					tabButton.className = SELECTED;
				}
				this.element.appendChild(tabButton);
			}
		},
		switchTab: function(element) {
			var tabButton = this.element.querySelectorAll("a");
			for(var index = 0; index < tabButton.length; index++) {
				tabButton[index].className = "";
			}
			element.className = SELECTED;
		}
	},
	page: {
		directory: {
			currentAddress: [],
			haveLogo: false,
			haveButtons: false,
			element: null,
			makeList: function(address) {
				var backButtonEnabled = false;
				if(address.length > 0)
					backButtonEnabled = true;
				if(address.length >= 1) {
					var stringAddress = "b_" + address[0];
					if(address.length > 1)
						for(var index = 1; index < address.length; index++)
							stringAddress += ("_" + address[index]);
				} else {
					var stringAddress = "b";
				}
				var buttons = [];
				for(var buttonId in data.button) {
					var current = (stringAddress == buttonId);
					var arrayAddress = buttonId.split("_");
					var subdirectory = (arrayAddress.length - address.length == 2);
					var valid = (buttonId.indexOf(stringAddress) == 0);
					var pushIt = false;
					if(valid) {
						var buttonElement = document.createElement("a");
						buttonElement.className = MOBILE_BUTTON;
						if(current && backButtonEnabled) {
							buttonId = BACK_BUTTON_ID;
							buttonElement.innerText = data.button[buttonId];
							pushIt = true;
						} else if(subdirectory) {
							buttonElement.innerText = data.button[buttonId];
							pushIt = true;
						}
						buttonElement.id = buttonId;
						buttonElement.onclick = function() {
							if(this.id == BACK_BUTTON_ID) {
								manual.page.directory.currentAddress.pop(manual.page.directory.currentAddress.length - 1);
							}	
							if(this.id.indexOf("b") == 0) {
								var choosedAddress = this.id.split("_");
								choosedAddress.shift();
								manual.page.directory.currentAddress = choosedAddress;
							}
							manual.page.directory.makeList(manual.page.directory.currentAddress);
						}
						if(pushIt)
							buttons.push(buttonElement);
					}
				}
				if(buttons.length > 1) {
					var buttonsContainer = this.element.querySelector(DIRECTORY_BUTTONS);
					clear(buttonsContainer);
					for(var index = 0; index < buttons.length; index++) {
						try {
							buttonsContainer.appendChild(buttons[index]);
						} catch(e) {
							console.log(e);
						}
					}
				}
				
				if(buttons.length == 1) {
					hide(this.element);
					manual.page.view.address = address;
					with(manual.page.view) {
						show(element);
						make();
					}
				}
			}
		},
		view: {
			element: null,
			address: [],
			make: function() {
				var address = this.address;
				var stringAddress = address[0];
				if(address.length > 1)
					for(var index = 1; index < address.length; index++) {
						stringAddress += "_" + address[index];
					}
				var selectedDocument = document.querySelector(DOCUMENT_ID_PREFIX + stringAddress);
				if(selectedDocument) {
					var allDocuments = document.querySelectorAll("#documents article");
					hideAll(allDocuments);
					show(selectedDocument);
				} else {
					alert(data.textId.document_not_found);
					hide(this.element);
					with(manual.page.directory) {	
						currentAddress = [];
						makeList(currentAddress);
						show(element);
					}
				}
			},
			goBack: function() {
				hide(manual.page.view.element);
				var backAddress = manual.page.view.address;
				backAddress.pop();
				with(manual.page.directory) {
					currentAddress = backAddress;
					makeList(currentAddress);
					show(element);
				}
			}
		},
		currentWork: {
			init: function() {
				var buttonsList = document.querySelectorAll(PREWORK_BUTTONS_LIST + " a");
				for(var i = 0; i < buttonsList.length; i++) {
					buttonsList[i].onclick = function() {
						hide(document.querySelector(PREWORK_WINDOW));
						show(document.querySelector(SHOW_WORK_ID));
					}
				}
			},
			element: null,
			data: {
				Q: 0,
				P: 0,
				I: 0,
				T: 0
			},
			schemeType: null,
			switchType: function(selectedElement) {
				var schemeTypes = document.querySelectorAll(SCHEME_TYPE_ID + " a");
				for(var index = 0; index < schemeTypes.length; index++) {
					schemeTypes[index].className = "";
				}
				selectedElement.className = SELECTED;
			},
			make: function() {
				var values = this.data;
				var body = this.element;
				var schemeTypeList = document.querySelectorAll(SCHEME_TYPE_ID + " a");
				var inputs = {
					Q: document.querySelector("#value-q"),
					P: document.querySelector("#value-p"),
					I: document.querySelector("#value-i"),
					T: document.querySelector("#value-t")
				}
				show(document.querySelector(INPUT_FORM));
				hide(document.querySelector(RESULTS));
				for(var index = 0; index < schemeTypeList.length; index++) {
					var schemeType = schemeTypeList[index];
					schemeType.setAttribute("data", index);
					schemeType.onclick = function() {
						with(manual.page.currentWork) {
							switchType(this);
							schemeType = +this.getAttribute("data");
							updateValuesVisibility();
						}
					}
				}
				this.setValuesFromData(this.data, inputs, schemeTypeList);
				var submitButton = document.querySelector("#work-submit");
				submitButton.onclick = function() {
					var saveResult = manual.page.currentWork.checkAndSave();
					if(saveResult) {
						with(manual.page.currentWork) {
							data = saveResult;
							processData();
						}
					} else {
						sendError(data.textId.wrong_input_values, function() {
							manual.page.currentWork.make();
						});
					}
				}
				this.archive.print();
				this.updateValuesVisibility();
			},
			updateValuesVisibility: function() {
				var cname;
				switch(manual.page.currentWork.schemeType) {
					case 0:
					case 1:
						cname = "knsppd";
						break;
					case 2:
						cname = "cns";
						break;
					default:
						cname = false;
				}
				var allValues = document.querySelectorAll(".values-row");
				for(var i = 1; i < allValues.length; i++) {
					allValues[i].style.display = "none";
				}
				if(cname) {	
					var showingValues = document.querySelectorAll("." + cname);
					for(var i = 0; i < showingValues.length; i++) {
						showingValues[i].style.display = "";
					}
				}
			},
			checkAndSave: function() {
				var qInput = document.querySelector("#value-q");
				var pInput = document.querySelector("#value-p");
				var iInput = document.querySelector("#value-i");
				var tInput = document.querySelector("#value-t");
				if(qInput && pInput && iInput && tInput) {
					var qValue = +qInput.value;
					var pValue = +pInput.value;
					var iValue = +iInput.value;
					var tValue = +tInput.value;
					if(isFinite(qValue) && isFinite(pValue) && isFinite(iValue) && isFinite(tValue)) {
						return {Q: qValue, P: pValue, I: iValue, T: tValue};
					} else {
						return false;
					}
				} else {
					sendError(data.textId.something_went_wrong);
					console.log("No iunput values elements.");
					return false;
				}
			},
			setValuesFromData: function(dataObject, inputs, typesList) {
				for(var id in dataObject) {
					if(inputs[id]) {
						inputs[id].value = dataObject[id];
					} else {
						console.log("Trying to set undefined value.");
						console.log("ID: " + id);
						console.log("Object: ");
						console.dir(inputs[id]);
					}
				}
				if(this.schemeType) {
					typesList[this.schemeType].className = SELECTED;
				}
			},
			processData: function(options) {
				function pushResult(problem, chance) {
					var rra = false;
					if(typeof problem == "object")
						rra = true;
					var resultItem = {
						text: problem,
						rra: rra
					};
					if(chance) {
						resultItem.chance = chance;
					}
					resultsArray.push(resultItem);
				}

				var Q = this.data.Q;
				var P = this.data.P;
				var I = this.data.I;
				var T = this.data.T;
				var resultsArray = [];
				var noError = true;
				var problem = data.problem;
				if(!options)
					var options = {};

				function kns() {
					if(Q < 235) {
						if(P > 155) {
							pushResult(problem.padenie_plashki, 100);
						} else if(between(P, 145, 155)) {
							pushResult(problem.padenie_plashki, 60);
							pushResult(problem.chastichnoe_perekrytie, 40);
						} else if(between(P, 143, 145)) {
							pushResult(problem.izmenenie_zakachka, 100);
						} else if(P < 143) {
							pushResult(problem.neispravnost_nasosnogo, 100);
						} else {
							noError = this.error();
						}
					} else if(between(Q, 235, 260)) {
						if(P > 145) {
							pushResult(problem.chastichnoe_perekrytie, 70);
							pushResult(problem.padenie_plashki, 30);
						} else if(between(P, 140, 145)) {
							pushResult(problem.object_rabotaet_ppd);
						} else if(P < 140) {
							pushResult(problem.neispravnost_nasosnogo, 70);
							pushResult(problem.proryv_napornogo, 30);
						} else {
							noError = this.error();
						}
					} else if(Q > 260) {
						if(P < 138) {
							pushResult(problem.proryv_napornogo, 100);
						} else if(between(P, 138, 143)) {
							pushResult(problem.proryv_napornogo, 60);
							pushResult(problem.izmenenie_zakachka, 40);
						} else if(P > 143) {
							pushResult(problem.neispravnost_kip, 100);
						} else {
							noError = this.error();
						}
					} else {
						noError = this.error();
					}
				}

				function ppd() {
					if(Q < 235) {
						if(P < 143) {
							pushResult(problem.smotri_kns, 100);
						} else if(between(P, 143, 145)) {
							pushResult(problem.ogranichenie_fonda, 100);
						} else if(P > 145) {
							pushResult(problem.smotri_kns, 100);
						} else {
							noError = this.error();
						}
					} else if(between(Q, 235, 260)) {
						if(P > 145) {
							pushResult(problem.smotri_kns, 100);
						} else if(between(P, 140, 142)) {
							pushResult(problem.otkrytie_skvazhin, 40);
							pushResult(problem.cyklicheskaya_zakachka, 40);
							pushResult(problem.snyatie_shtucera, 20);
						} else if(betweenRI(P, 142, 143)) {
							pushResult(problem.object_rabotaet);
						} else if(betweenRI(P, 143, 145)) {
							pushResult(problem.cyklicheskaya_zakachka, 40);
							pushResult(problem.ogranichenie_fonda, 40);
							pushResult(problem.ustanovka_shtucera, 20);
						} else if(P < 140) {
							pushResult(problem.smotri_kns, 100);
						} else {
							noError = this.error();
						}
					} else if(Q > 260) {
						if(P < 138) {
							pushResult(problem.smotri_kns, 100);
						} else if(between(P, 138, 143)) {
							pushResult(problem.smotri_kns, 60);
							pushResult(problem.otkrytie_skvazhin, 20);
							pushResult(problem.cyklicheskaya_zakachka, 20);
						} else if(P > 143) {
							pushResult(problem.smotri_kns, 100);
						} else {
							noError = this.error();
						}
					} else {
						noError = this.error();
					}
				}

				function cns() {
					if(I < 140) {
						pushResult(problem.normalny_rezhim_toka);
					} else {
						pushResult(problem.nizkoe_napryazhenie);
						pushResult(problem.povyshenny_rashod);
					}

					if(T < 70) {
						pushResult(problem.normalny_rezhim_temp);
					} else {
						pushResult(problem.nizkoe_davlenie);
						pushResult(problem.neispravnost_maslonasosa);
					}
				}

				switch(this.schemeType) {
					case 0:
						kns();
						break;
					case 1:
						ppd();
						break;
					case 2:
						cns();
						break;
					default:
						noError = sendError(data.textId.type_not_choosen, function() {
							manual.page.currentWork.make();
						});
				}

				if(noError) {
					if(!options.dontSave) {	
						var date = new Date();
						var varsId = this.archive.diagramValues(this.schemeType);
						this.archive.push({
							schemeType: this.schemeType,
							vars: [
								this.data[varsId[0]],
								this.data[varsId[1]]
							]
						});
					}
					this.printResults(resultsArray);
				}
			},
			error: function() {
				var afterError = function() {
					manual.page.currentWork.make();
				}
				return sendError(data.textId.diagram_error, afterError);
			},
			printResults: function(results) {
				if(results && results.length >= 1) {
					var body = document.querySelector(RESULTS);
					clear(body);
					var header = document.createElement("h1");
					header.innerText = data.textId.results;
					var restartButton = document.createElement("a");
					restartButton.className = MOBILE_BUTTON;
					restartButton.innerText = data.textId.restart_again;
					restartButton.onclick = function() {
						manual.page.currentWork.make();
					}
					body.appendChild(header);
					for(var index = 0; index < results.length; index++) {
						var result = results[index];
						var resultElement = document.createElement("div");
						resultElement.className = "result-box";
						var problem = document.createElement("div");
						problem.className = "problem";
						if(!result.rra) {
							problem.innerText = result.text;
						} else {
							var code = "<table>";
							var ids = [
								"reason",
								"investigation",
								"action"
							];
							for(var i = 0; i < ids.length; i++) {
								var id = ids[i];
								code += "<tr><th>" + data.textId[id] + "</th><td>" + result.text[id] + "</td></tr>";
							}
							code += "</table>";
							problem.innerHTML = code;
						}
						resultElement.appendChild(problem);
						if(result.chance) {
							var chanceContainer = document.createElement("div");
							chanceContainer.className = "progress-bar";
							var chanceLine = document.createElement("div");
							chanceLine.innerText = result.chance + "%";
							chanceLine.className = "progress-line";
							chanceLine.style.width = result.chance + "%";
							chanceContainer.appendChild(chanceLine);
							resultElement.appendChild(chanceContainer);
						}
						body.appendChild(resultElement);
						body.appendChild(restartButton);
					}
					hide(document.querySelector(INPUT_FORM));
					show(body);
				} else {
					sendError(data.textId.cant_print_results);
				}
			},
			archive: {
				current: [
				],
				buttonEnabled: true,
				init: function() {
					document.querySelector(SHOW_ARHIVE_ID).onclick = function(element) {
						var archive = manual.page.currentWork.archive;
						if(archive.buttonEnabled) {	
							archive.buttonEnabled = false;
							var archiveView = document.querySelector(ELEMENT_ARCHIVE_VIEW);
							switch(archiveView.className) {
								case "show":
									archiveView.className = "hide";
									setTimeout(function() {
										document.querySelector(ELEMENT_ARCHIVE_VIEW).style.display = "none";
									}, 750);
									break;
								case "hide":
								default:
									archiveView.style.display = "block";
									setTimeout(function() {
										document.querySelector(ELEMENT_ARCHIVE_VIEW).className = "show";
									}, 0);
							}
							setTimeout(function() {
								manual.page.currentWork.archive.buttonEnabled = true;
							}, 750);
						}
					}
					document.querySelector("#clear-archive").onclick = function() {
						manual.page.currentWork.archive.clear();
						manual.page.currentWork.make();
					}
					this.load();
				},
				print: function() {
					var view = document.querySelector(ELEMENT_ARCHIVE);
					view.innerHTML = "";
					var log = this.current;
					if(log.length > 0) {
						for(var i = 0; i < log.length; i++) {
							var save = log[i];
							var record = document.createElement("div");
							var id = document.createElement("div");
							var title = document.createElement("div");
							var top = document.createElement("div");
							var name = document.createElement("span");
							var date = document.createElement("span");
							var bottom = document.createElement("div");
							var vars = [
								document.createElement("span"),
								document.createElement("span")
							];

							record.className = "record";
							record.setAttribute("rid", i);
							record.onclick = function(elem) {
								var id = this.getAttribute("rid");
								var currentWork = manual.page.currentWork;
								var archive = currentWork.archive;
								var record = archive.current[id];

								currentWork.schemeType = record.schemeType;
								var varIds = archive.diagramValues(record.schemeType);
								for(var i = 0; i < record.vars.length; i++)
									currentWork.data[varIds[i]] = record.vars[i];

								currentWork.switchType(document.querySelectorAll(SCHEME_TYPE_ID + " a")[record.schemeType]);
								currentWork.processData({
									dontSave: true
								});

							}

							id.className = "number";
							id.innerText = save.id;
							title.className = "title";
							name.className = "name";
							name.innerText = document.querySelectorAll(SCHEME_TYPE_ID + " a")[save.schemeType].innerText;
							date.className = "date";
							date.innerText = (save.day.length < 2? "0": "") + save.day + "/";
							date.innerText += (save.month.length < 2? "0": "") + save.month;
							for(var v = 0; v < vars.length; v++) {
								var varName = document.createElement("span");
								var varValue = document.createElement("span");
								var varNames = this.diagramValues(save.schemeType);
								vars[v].className = "value";
								varName.innerText = varNames? varNames[v] : sendError(data.textId.record_corrupted);
								varValue.innerText = save.vars[v];
								vars[v].appendChild(varName);
								vars[v].appendChild(varValue);
								bottom.appendChild(vars[v]);
							}

							top.appendChild(name);
							top.appendChild(date);
							title.appendChild(top);
							title.appendChild(bottom);
							record.appendChild(id);
							record.appendChild(title);
							view.appendChild(record);
						}
					} else {
						view.innerText = "Записей в архиве пока нет.";
					}
				},
				save: function() {
					this.current.reverse();
					for(var i = 0; i < ARCHIVE_SIZE; i++) {
						var recordName = RECORD_PREFIX + i;
						localStorage.setItem(recordName, "");
						var record = this.current[i];
						if(record) {
							var str = "";
							var queue = [
								"id", "schemeType", "day", "month"
							];
							for(var n = 0; n < queue.length; n++) {
								str += (record[queue[n]] + SPLIT_SYMBOL);
							}
							str += record.vars[0] + SPLIT_SYMBOL;
							str += record.vars[1];
							localStorage.setItem(recordName, str);
						}
					}
					this.current.reverse();
				},
				load: function() {
					this.current = new Array();
					for(var i = 0; i < ARCHIVE_SIZE; i++) {
						var recordName = RECORD_PREFIX + i;
						var record = localStorage.getItem(recordName);
						if(record) {
							var recordArray = record.split(SPLIT_SYMBOL);
							if(recordArray.length == 6) {
								var save = {
									id: +recordArray[0],
									schemeType: +recordArray[1],
									day: recordArray[2],
									month: recordArray[3],
									vars: [
										+recordArray[4],
										+recordArray[5]
									]
								}
								this.current.unshift(save);
							} else {
								console.log("Wrong length at record #" + i + ".");
							}
						} else
							break;
					}
				},
				clear: function() {
					for(var i = 0; i < ARCHIVE_SIZE; i++) {
						var recordName = RECORD_PREFIX + i;
						localStorage.setItem(recordName, "");
					}
					localStorage.setItem(RECORD_PREFIX + ARCHIVE_LASTID_NAME, "");
					this.current = new Array();
				},
				diagramValues: function(schemeType) {
					switch(schemeType) {
						case 0:
						case 1:
							return ["Q", "P"];
							break;
						case 2:
							return ["I", "T"];
							break;
						default:
							return false;
					}
				},
				getId: function() {
					var lastIdName = RECORD_PREFIX + ARCHIVE_LASTID_NAME;
					var lastId = +localStorage.getItem(lastIdName);
					if(!isNaN(lastId) && lastId > 0) {
						lastId++;
					} else {
						lastId = 1;
					}
					localStorage.setItem(lastIdName, lastId);
					return lastId;
				},
				push: function(data) {
					var date = new Date();
					var save = {
						id: this.getId(),
						schemeType: data.schemeType,
						day: (date.getDate() + 1) + "",
						month: (date.getMonth() + 1) + "",
						vars: data.vars
					}
					this.current.unshift(save);
					if(this.current.length > ARCHIVE_SIZE) {
						this.current.pop();
					}
					this.save();
				}
			}
		}
	}
}
function hide(e) {
	e.style.display = "none";
}
function hideAll(elements) {
	for(var id = 0; id < elements.length; id++) {
		hide(elements[id])
	}
}
function show(e) {
	e.style.display = "block";
}
function clear(e) {
	e.innerHTML = "";
}
function between(v, f, s) {
	return (v >= Math.min(f, s) && v <= Math.max(f, s));
}
function betweenRI(v, f, s) {
	return (v > Math.min(f, s) && v <= Math.max(f, s));
}
function sendError(text, onerror) {
	if(text)
		alert(text);
	else
		alert(data.textId.something_went_wrong);
	if(onerror)
		onerror();
	return false;
}
var globalNotification = false;
function checkUpdates() {
	if(!globalNotification) {
		globalNotification = true;
		setTimeout(function() {
			document.querySelector(NOTIFICATION).style.display = "block";
			document.querySelector(NOTIFICATION_TEXT).innerText = "Проверка наличия обновлений…";
			setTimeout(function() {
				document.querySelector(NOTIFICATION).className = "show";
			}, 1);
			setTimeout(function() {
				document.querySelector(NOTIFICATION).className = "";
				setTimeout(function() {
					document.querySelector(NOTIFICATION_TEXT).innerText = "У вас последняя версия справочного материала.";
					setTimeout(function() {
						document.querySelector(NOTIFICATION).className = "show";
					}, 1);
					setTimeout(function() {
						document.querySelector(NOTIFICATION).className = "";
						setTimeout(function() {
							document.querySelector(NOTIFICATION).style.display = "none";
							globalNotification = false;
						}, 500)
					}, 2500)
				}, 500)
			}, 5000)
		}, 1000);
	}
} 